(function () {

	angular.module('TevalClient').directive('evaluationQuestion', function() {
		return {
			restrict: 'E',
			require: '^ngModel',
			scope: {
				ngModel: '='
			},
			templateUrl: 'views/questiontemplate.html'
		};
	});
}());