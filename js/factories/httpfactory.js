(function () {

	angular.module('TevalClient').factory('HttpFactory',
	['$http', 'SERVER_URL',
	function ($http, SERVER_URL) {
		var token = '';
		var user = '';
		var url = SERVER_URL;

		function login(userObj) {
			return $http.post(url+'login', userObj)
				.then(function (response) {
					if (response.data.Token) {
						token = response.data.Token;
						user = response.data.User;
					}
				});
		}
		function logout() {
			token = '';
			user = '';
		}
		function getToken() {
			return token;
		}
		function getUser() {
			return user;
		}
		function getEvaluations() {
			return $http.get(url+'evaluations');
		}
		function getEvaluation(id) {
			return $http.get(url+'evaluations/'+id);
		}
		function postEvaluation(evalObj) {
			return $http.post(url+'evaluations', evalObj)
				.then(function (response) { });
		}
		function getEvaluationTemplates() {
			return $http.get(url+'evaluationtemplates');
		}
		function getEvaluationTemplate(id) {
			return $http.get(url+'evaluationtemplates/'+id);
		}
		function postEvaluationTemplate(tempObj) {
			return $http.post(url+'evaluationtemplates', tempObj)
				.then(function (response) { });
		}
		function getEvaluationResults(id) {
			return $http.get(url+'evaluations/'+id);
		}
		function getMyEvaluations() {
			return $http.get(url+'my/evaluations');
		}
		function getMyEvaluation(route) {
			return $http.get(url+'courses/'+route.CourseID+"/"+route.Semester+"/evaluations/"+route.ID);
		}
		function postMyEvaluation(route, evalObj) {
			return $http.post(url+'courses/'+route.CourseID+"/"+route.Semester+"/evaluations/"+route.ID, evalObj);
		}
		function getMyCourseTeachers(route) {
			return $http.get(url+'courses/'+route.CourseID+"/"+route.Semester+"/teachers");
		}

		return {
			login: login,
			logout: logout,
			token: getToken,
			user: getUser,
			evaluations: getEvaluations,
			evaluation: getEvaluation,
			newEvaluation: postEvaluation,
			evaluationTemplates: getEvaluationTemplates,
			evaluationTemplate: getEvaluationTemplate,
			newEvaluationTemplate: postEvaluationTemplate,
			evaluationResults: getEvaluationResults,
			myEvaluations: getMyEvaluations,
			myEvaluation: getMyEvaluation,
			submitAnswers: postMyEvaluation,
			myCourseTeachers: getMyCourseTeachers
		};
	}]);
}());