(function () {

	angular.module('TevalClient').provider('AuthInterceptor',
	function () {
		var provider = {};

		provider.$get = function ($q, $injector) {
			var token = '';
			return {
				"request": function (config) {
					$injector.invoke(function ($http, HttpFactory) {
						token = HttpFactory.token();
						if (token !== '') {
							config.headers.Authorization = "Basic " + token;
						}
					});
					return config || $q.when(config);
				},
				"requestError": function (rejection) {
					return $q.reject(rejection);
				},
				"response": function (response) {
					return response || $q.when(response);
				},
				"responseError": function (rejection) {
					return $q.reject(rejection);
				}
			};
		};

		return provider;
	});
}());