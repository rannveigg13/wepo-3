(function () {

	angular.module('TevalClient').controller('LoginController',
	['$scope', '$location', '$rootScope', '$routeParams', '$http', 'HttpFactory',
	function ($scope, $location, $rootScope, $routeParams, $http, HttpFactory) {
		$scope.errorMessage = '';
		$scope.nickname = '';
		$scope.token = HttpFactory.token();
		if ($scope.token !== '') {
			$location.path('/my');
		}

		$scope.login = function () {
			var userObj = {
				"user": $scope.nickname,
				"pass": "123456"
			};
			HttpFactory.login(userObj).then(function () {
				$location.path('/my');
			});
		};
	}]);
}());