(function () {

	angular.module('TevalClient').controller('AnswerController',
	['$scope', '$location', '$rootScope', '$routeParams', 'HttpFactory',
	function ($scope, $location, $rootScope, $routeParams, HttpFactory) {
		$scope.errorMessage = '';
		$scope.courseId = $routeParams.course;
		$scope.semester = $routeParams.semester;
		$scope.evalId = $routeParams.id;
		$scope.myEvaluation = {};
		$scope.question = '';
		$scope.allQuestions = [];
		$scope.results = [];
		$scope.teachers = [];

		
		// user authentication
		$scope.token = HttpFactory.token();
		$scope.user = HttpFactory.user();
		$scope.role = $scope.user.Role;
		if ($scope.token === '') {
			$location.path("/login");
		} else {
			if ($scope.role === 'student') {
				var routeObj = {
					"CourseID": $scope.courseId,
					"Semester": $scope.semester,
					"ID": $scope.evalId
				};
				HttpFactory.myCourseTeachers( {"CourseID": $scope.courseId, "Semester": $scope.semester} ).then(function (response) {
					$scope.teachers = response.data;
				});
				HttpFactory.myEvaluation(routeObj).then(function (response) {
					var evaluation = response.data;
					$scope.myEvaluation = evaluation;
					var tQuestions = evaluation.TeacherQuestions;
					$scope.allQuestions = evaluation.CourseQuestions;
					angular.forEach($scope.teachers, function (teacher) {
						angular.forEach(tQuestions, function (question) {
							$scope.allQuestions.push( {"Question": question, "Teacher": teacher} );
						});
					});
				});
			}
		}

		/* Answer arrays not supported by API */
		var multipleChoiceParser = function (answerObj) {
			var keys = [];
			for (var key in answerObj) {
				if (answerObj.hasOwnProperty(key)) {
					keys.push(key);
				}
			}
			var ansArr = [];
			for (var i = 0; i < keys.length; i++) {
				if (answerObj[keys[i]] === true) {
					ansArr.push(parseInt(keys[i]));
				}
			}
			return multipleToSingleAnswerFix(ansArr);
		};
		var multipleToSingleAnswerFix = function (answerArray) {
			return answerArray[0];
		};

		$scope.submitEvaluation = function () {
			angular.forEach($scope.allQuestions, function (obj) {
				// course question
				if (obj.Teacher === undefined) {
					var cans = {
						"QuestionID": obj.ID
					};
					if (typeof(obj.answer) === "object") {
						cans["Value"] = multipleChoiceParser(obj.answer);
					} else {
						cans["Value"] = obj.answer;
					}
					$scope.results.push(cans);
				} 
				// teacher question
				else {
					var tans = {
						"QuestionID": obj.Question.ID,
						"TeacherSSN": obj.Teacher.SSN
					};
					if (typeof(obj.answer) === "object") {
						tans["Value"] = multipleChoiceParser(obj.answer);
					} else {
						tans["Value"] = obj.answer;
					}
					$scope.results.push(tans);
				}
			});
			HttpFactory.submitAnswers(routeObj, $scope.results).then(function (response) {
				$location.path("/my");
			});
			// call HttpFactory to post evaluation answers
		};

	}]);
}());