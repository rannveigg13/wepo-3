(function () {

	angular.module('TevalClient').controller('MyController',
	['$scope', '$location', '$rootScope', '$routeParams', 'HttpFactory',
	function ($scope, $location, $rootScope, $routeParams, HttpFactory) {
		$scope.errorMessage = '';
		$scope.evaluationTemplates = [];
		$scope.evaluations = [];
		$scope.myEvaluations = [];
		$scope.selectOptions = [];
		$scope.startDate = new Date();
		$scope.endDate = new Date();
		$scope.currDate = '';
		$scope.selectedTemplate = '';
		
		// user authentication
		$scope.token = HttpFactory.token();
		$scope.user = HttpFactory.user();
		$scope.role = $scope.user.Role;
		if ($scope.token === '') {
			$location.path("/login");
		} else {
			if ($scope.role === 'admin') {
				HttpFactory.evaluationTemplates().then(function (response) {
					$scope.evaluationTemplates = response.data;
					var templates = $scope.evaluationTemplates;
					for (var i = 0; i < templates.length; i++) {
						$scope.selectOptions.push(templates[i].ID);
					}
				});
				HttpFactory.evaluations().then(function (response) {
					$scope.evaluations = response.data;
					$scope.currDate = (new Date()).toISOString();
				});
			} else if ($scope.role === 'student') {
				HttpFactory.myEvaluations().then(function (response) {
					$scope.myEvaluations = response.data;
				});
			}
		}

		$scope.addEvaluation = false;

		$scope.getTime = function () {
			$scope.currTime = new Date();
		};
		$scope.logout = function () {
			HttpFactory.logout();
			$location.path("/login");
		};
		$scope.showCourses = function () {
			$scope.courses = HttpFactory.courses();
		};
		$scope.showEvaluations = function () {
			if ($scope.role === 'admin') {
				HttpFactory.evaluations().then(function (response) {
					$scope.evaluations = response.data;
				});
			}
		};
		$scope.showEvaluation = function (id) {
			if ($scope.role === 'admin') {
				HttpFactory.evaluation(id).then(function (response) {
					$scope.currentEvaluation = response.data;
				});
			}
		};
		var validateEvaluation = function (e) {
			var now = $scope.currDate;
			if (e.TemplateID !== '' && e.StartDate !== '' && e.EndDate >= '' && e.EndDate > now) {
				return true;
			}
			else if (e.TemplateID === '') {
				$scope.errorMessage = "Please choose evaluation template";
			}
			else if (e.StartDate === '') {
				$scope.errorMessage = "Please enter start date for evaluation";
			}
			else if (e.EndDate === '') {
				$scope.errorMessage = "Please enter end date for evaluation";
			}
			if (e.EndDate <= now) {
				$scope.errorMessage = "Please choose an end date in the future";
			}
			return false;
		};
		$scope.sendEvaluation = function () {
			if ($scope.role === 'admin') {
				var evaluation = {
					"TemplateID": $scope.selectedTemplate,
					"StartDate": $scope.startDate.toISOString(),
					"EndDate": $scope.endDate.toISOString()
				};
				if (validateEvaluation(evaluation)) {
					HttpFactory.newEvaluation(evaluation).then(function (response) {
						$scope.addEvaluation = false;
						HttpFactory.evaluations().then(function (response) {
							$scope.evaluations = response.data;
						});
					});
				}
			}
		};
		$scope.showEvaluationTemplates = function () {
			if ($scope.role === 'admin') {
				HttpFactory.evaluationTemplates().then(function (response) {
					$scope.evaluationTemplates = response.data;
				});
			}
		};
		$scope.showEvaluationTemplate = function (id) {
			if ($scope.role === 'admin') {
				HttpFactory.evaluationTemplate(id).then(function (response) {
					$scope.currentEvaluationTemplate = response.data;
				});
			}
		};
		$scope.sendEvaluationTemplate = function (template) {
			if ($scope.role === 'admin') {
				HttpFactory.newEvaluationTemplate(template);
			}
		};
		$scope.createTemplate = function () {
			if ($scope.role === 'admin') {
				$location.path('/newTemplate');
			}
		};
		$scope.createEvaluation = function () {
			$scope.addEvaluation = true;
			$scope.startDate = new Date();
			$scope.endDate = new Date($scope.startDate.getTime() + 2592000000);
		};
		$scope.answer = function (evalObj) {
			$location.path("/evaluations/"+evalObj.CourseID+"/"+evalObj.Semester+"/"+evalObj.ID);
		};
		$scope.showEvalAnswers = function (evalId) {
			$location.path("/results/"+evalId);
		};
	}]);
}());