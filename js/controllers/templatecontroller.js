(function () {

	angular.module('TevalClient').controller('TemplateController',
	['$scope', '$location', '$rootScope', '$routeParams', 'HttpFactory',
	function ($scope, $location, $rootScope, $routeParams, HttpFactory) {
		$scope.errorMessage = '';
		$scope.titleIs = '';
		$scope.titleEn = '';
		$scope.introIs = '';
		$scope.introEn = '';
		$scope.cQuestions = [];
		$scope.cChoices = [];
		$scope.tQuestions = [];
		$scope.tChoices = [];
		$scope.selectOptions = ['text', 'single', 'multiple'];
		$scope.selectedCourseType = $scope.selectOptions[0];
		$scope.selectedTeacherType = $scope.selectOptions[0];
		$scope.cText = '';
		$scope.cTextEn = '';
		$scope.cImageUrl = '';
		$scope.cWeight = 0;
		$scope.tText = '';
		$scope.tTextEn = '';
		$scope.tImageUrl = '';
		$scope.tWeight = 0;
		$scope.cChoiceID = 0;
		$scope.cQuestionID = 0;
		$scope.tChoiceID = 0;
		$scope.tQuestionID = 0;
		$scope.cAnsText = '';
		$scope.cAnsTextEn = '';
		$scope.cAnsImageUrl = '';
		$scope.cAnsWeight = '';
		$scope.tAnsText = '';
		$scope.tAnsTextEn = '';
		$scope.tAnsImageUrl = '';
		$scope.tAnsWeight = '';
		
		// user authentication
		$scope.token = HttpFactory.token();
		if ($scope.token === '') {
			$location.path("/login");
		}
		$scope.user = HttpFactory.user();
		$scope.role = $scope.user.Role;

		$scope.getTime = function () {
			$scope.currTime = new Date();
		};

		var validateQuestion = function (q) {
			if (q.Text !== '' && q.TextEN !== '' && q.Type !== '') {
				return true;
			}
			else if (q.Text === '') {
				$scope.errorMessage = "Please enter question Text";
			}
			else if (q.TextEN === '') {
				$scope.errorMessage = "Please enter question Text in English";
			}
			else if (q.Type === '') {
				$scope.errorMessage = "Please enter Type of question";
			}
			return false;
		};

		$scope.addCourseQuestion = function () {
			var questionObj = {
				"ID": $scope.cQuestionID,
				"Text": $scope.cText,
				"TextEN": $scope.cTextEn,
				"ImageURL": $scope.cImageUrl,
				"Type": $scope.selectedCourseType
			};
			if (validateQuestion(questionObj)) {
				if ($scope.selectedCourseType !== 'text') {
					questionObj["Answers"] = $scope.cChoices;
				}
				$scope.cQuestions.push(questionObj);
				$scope.cChoices = [];
				$scope.cQuestionID++;
				$scope.cChoiceID = 0;
				$scope.cText = '';
				$scope.cTextEn = '';
				$scope.cImageUrl = '';
				$scope.selectedCourseType = 'text';
			}
		};

		$scope.addTeacherQuestion = function () {
			var questionObj = {
				"ID": $scope.tQuestionID,
				"Text": $scope.tText,
				"TextEN": $scope.tTextEn,
				"ImageURL": $scope.tImageUrl,
				"Type": $scope.selectedTeacherType
			};
			if (validateQuestion(questionObj)) {
				if ($scope.selectedTeacherType !== 'text') {
					questionObj["Answers"] = $scope.tChoices;
				}
				$scope.tQuestions.push(questionObj);
				$scope.tChoices = [];
				$scope.tQuestionID++;
				$scope.tChoiceID = 0;
				$scope.tText = '';
				$scope.tTextEn = '';
				$scope.tImageUrl = '';
				$scope.selectedTeacherType = 'text';
			}
		};

		var validateTemplate = function (t) {
			if (t.Title !== '' && t.TitleEN !== '' && t.IntroText !== '' && t.IntroTextEN !== '' && t.TeacherQuestions.length > 0) {
				return true;
			}
			else if (t.Title === '') {
				$scope.errorMessage = "Please enter template Title";
			}
			else if (t.TitleEN === '') {
				$scope.errorMessage = "Please enter template Title in English";
			}
			else if (t.IntroText === '') {
				$scope.errorMessage = "Please enter template Introtext";
			}
			else if (t.IntroTextEN === '') {
				$scope.errorMessage = "Please enter template Introtext in English";
			}
			else if (t.TeacherQuestions.length === 0) {
				$scope.errorMessage = "Please add at least 1 teacher question";
			}
			return false;
		};
		$scope.submitTemplate = function () {
			var tempObj = {
				"Title": $scope.titleIs,
				"TitleEN": $scope.titleEn,
				"IntroText": $scope.introIs,
				"IntroTextEN": $scope.introEn,
				"CourseQuestions": $scope.cQuestions,
				"TeacherQuestions": $scope.tQuestions
			};
			if (validateTemplate(tempObj)) {
				HttpFactory.newEvaluationTemplate(tempObj).then(function (response) {
					$location.path('/my');
				});
			}
		};

		var validateChoice = function (c) {
			if (c.Text !== '' && c.TextEN !== '' && c.Weight !== '') {
				return true;
			}
			else if (c.Text === '') {
				$scope.errorMessage = "Please enter choice Text";
			}
			else if (c.TextEN === '') {
				$scope.errorMessage = "Please enter choice Text in English";
			}
			else if (c.Weight === '') {
				$scope.errorMessage = "Please enter Weight of choice";
			}
			return false;
		};
		$scope.addCourseQuestionChoice = function () {
			var choiceObj = {
				"ID": $scope.cChoiceID,
				"Text": $scope.cAnsText,
				"TextEN": $scope.cAnsTextEn,
				"ImageURL": $scope.cAnsImageUrl,
				"Weight": $scope.cAnsWeight
			};
			if (validateChoice(choiceObj)) {
				$scope.cChoices.push(choiceObj);
				$scope.cChoiceID++;
				$scope.cAnsText = '';
				$scope.cAnsTextEn = '';
				$scope.cAnsImageUrl = '';
				$scope.cAnsWeight = '';
			}
		};
		$scope.addTeacherQuestionChoice = function () {
			var choiceObj = {
				"ID": $scope.tChoiceID,
				"Text": $scope.tAnsText,
				"TextEN": $scope.tAnsTextEn,
				"ImageURL": $scope.tAnsImageUrl,
				"Weight": $scope.tAnsWeight
			};
			if (validateChoice(choiceObj)) {
				$scope.tChoices.push(choiceObj);
				$scope.tChoiceID++;
				$scope.tAnsText = '';
				$scope.tAnsTextEn = '';
				$scope.tAnsImageUrl = '';
				$scope.tAnsWeight = '';
			}
		};
	}]);
}());