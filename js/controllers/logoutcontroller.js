(function () {

	angular.module('TevalClient').controller('LogoutController',
	['$scope', '$location', '$rootScope', '$routeParams', 'HttpFactory',
	function ($scope, $location, $rootScope, $routeParams, HttpFactory) {
		$scope.errorMessage = '';
		HttpFactory.logout();
		$location.path("/login");
	}]);
}());