(function () {

	angular.module('TevalClient').controller('ResultsController',
	['$scope', '$location', '$rootScope', '$routeParams', 'HttpFactory',
	function ($scope, $location, $rootScope, $routeParams, HttpFactory) {
		$scope.errorMessage = '';
		$scope.evalId = $routeParams.id;
		$scope.evaluation = {};
		$scope.teachers = [];
		$scope.selectedCourse = 0;
		$scope.selectedSemester = 0;
		$scope.selectedQuestion = 0;
		$scope.selectedTeacher = "";
		$scope.chart = {};
		$scope.chartShown = false;

		
		// user authentication
		$scope.token = HttpFactory.token();
		$scope.user = HttpFactory.user();
		$scope.role = $scope.user.Role;
		if ($scope.token === '') {
			$location.path("/login");
		} else {
			if ($scope.role === 'admin') {
				HttpFactory.evaluationResults($scope.evalId).then(function (response) {
					$scope.evaluation = response.data;
				});
			}
		}

		var teacherSSNToObj = function (teachers, ssn) {
			for (var it = 0; it < teachers.length; it++) {
				if (teachers[it].SSN === ssn) {
					return teachers[it];
				}
			}
		};

		$scope.showEval = function (id) {
			$scope.chartShown = false;
			$scope.chart = {};
			$scope.allQuestions = [];
			$scope.selectedCourse = id;
			var index = -1;
			for (var i = 0; i < $scope.evaluation.Courses.length; i++) {
				if (id === $scope.evaluation.Courses[i].CourseID) {
					$scope.selectedSemester = $scope.evaluation.Courses[i].Semester;
					index = i;
				}
			}
			HttpFactory.myCourseTeachers( { CourseID: id, Semester: $scope.selectedSemester } ).then(function (teachersResponse) {
				$scope.teachers = teachersResponse.data;
				for (var j = 0; j < $scope.evaluation.Courses[index].Questions.length; j++) {
			 		var question = $scope.evaluation.Courses[index].Questions[j];
			 		if (question.TeacherSSN === null) { // Course Question
			 			$scope.allQuestions.push(question);
			 		} else { // Teacher Question
			 			var teacher = teacherSSNToObj($scope.teachers, question.TeacherSSN);
				 		var qObj = {
				 			"Question": question,
				 			"Teacher": teacher
				 		};
				 		$scope.allQuestions.push(qObj);
			 		}
			 	}
			});
		};

		$scope.showChart = function (qObj) {
			$scope.selectedQuestion = qObj.QuestionID;
			$scope.selectedTeacher = qObj.TeacherSSN;
			var rowsArr = [];
			for (var i = 0; i < qObj.OptionsResults.length; i++) {
				var option = qObj.OptionsResults[i];
				rowsArr.push( { c: [ { v: option.AnswerText }, { v: option.Count } ] } );
			}
			var chartObj = {
				data: {
					"cols": [ {id: "testAns", label: "Answer", type: "string"}, {id: "testCount", label: "Count", type: "number"} ],
					"rows": rowsArr
				}
			};
			chartObj.type = "BarChart";
			chartObj.options = {
				'title': qObj.Text
			};
			$scope.chartShown = true;
			$scope.chart = chartObj;
		};
	}]);
}());