'use strict';

var TevalClient = angular.module('TevalClient', ['ng', 'ngRoute', 'googlechart']);

TevalClient.constant("SERVER_URL", "http://dispatch.ru.is/h06/api/v1/");


TevalClient.config(['$routeProvider', '$httpProvider', 'AuthInterceptorProvider',
	function ($routeProvider, $httpProvider, AuthInterceptorProvider) {
		$routeProvider
			.when('/login', { templateUrl: 'views/login.html', controller: 'LoginController' })
			.when('/my', { templateUrl: 'views/my.html', controller: 'MyController' })
			.when('/newTemplate', { templateUrl: 'views/newtemplate.html', controller: 'TemplateController' })
			.when('/evaluations/:course/:semester/:id', { templateUrl: 'views/answer.html', controller: 'AnswerController' })
			.when('/results/:id', { templateUrl: 'views/results.html', controller: 'ResultsController' })
			.when('/disconnect', { templateUrl: 'views/disconnect.html', controller: 'LogoutController' })
			.otherwise({
				redirectTo: '/my'
			});
		$httpProvider.interceptors.push('AuthInterceptor');
	}
]);