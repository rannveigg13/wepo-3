### Teaching Evaluation Application ###

* To install grunt run "npm install -g grunt-cli"

* Run "npm install -d" to install all dependencies
  - When prompted for angular version enter "2"!

* Concurrent watch:htmlhint and watch:jshint "grunt dev"

* Concat .js files: "grunt jscon"
  - Grunt watch task: "grunt watch:scripts"