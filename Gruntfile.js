module.exports = function(grunt){
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		htmlhint: {
			build: {
				options: {
					'tag-pair': true,
		            'tagname-lowercase': true,
		            'attr-lowercase': true,
		            'attr-value-double-quotes': true,
		            'doctype-first': false,
		            'spec-char-escape': true,
		            'id-unique': true,
		            'head-script-disabled': false,
		            'style-disabled': true
				},
				src: ['index.html', 'views/*.html']
			}
		},
		jshint: {
			options: {
				reporter: require('jshint-stylish'),
				curly: true,
				immed: true,
				newcap: true,
				noarg: true,
				sub: true,
				boss: true,
				eqnull: true,
				node: true,
				undef: true,
				globals: {
					_: false,
					jQuery: false,
					angular: false,
					moment: false,
					console: false,
					$: false,
					io: false
				},
				ignores: ['js/vendor/*.js']
			},
			dev: ['Gruntfile.js', 'js/**/*.js', '!js/vendor/**/*.js'],
			build: ['build/app.min.js']
		},
		less: {
			app: {
				files: {
					'build/main.css': 'css/main.less'
				}
			}
		},
		ngAnnotate: {
			options: {
				remove: true,
				singleQuotes: true
			},
			app: {
				files: {
					'build/app.min.js': ['js/**/*.js', '!js/vendor/**/*.js']
				}
			}
		},
		watch: {
			html: {
				files: ['index.html', 'views/*.html'],	
				tasks: ['htmlhint']
			},
			scripts: {
				files: ['js/**/*.js', '!js/vendor/**/*.js'],
				tasks: ['jscon']
			}
		},
		concurrent: {
			target: {
				tasks: ['watch:html', 'watch:scripts'],
				options: {
					logConcurrentOutput: true
				}
			}
		}
	});
	grunt.registerTask("test", ['htmlhint', 'jshint:dev']);
	grunt.registerTask("jscon", ['jshint:dev', 'ngAnnotate', 'jshint:build']);
	grunt.registerTask("default", ['htmlhint', 'jscon']);
	grunt.registerTask("dev", ['concurrent']);
};